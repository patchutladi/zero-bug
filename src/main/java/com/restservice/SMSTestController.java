package com.restservice;

import com.functions.AppProperties;
import com.functions.Logic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.rmi.runtime.Log;

@RestController
@Service
public class SMSTestController {

    @Autowired
    private  AppProperties appProperties;

    @GetMapping("/sendSMS")
    public String greeting(@RequestParam(value = "toNumber") String toNumber,
                            @RequestParam(value = "messageToSend") String messageToSend) {


        return Logic.sendSMS(toNumber,messageToSend);
    }

    @GetMapping("/requestWifi")
    public String requestWifi(@RequestParam(value = "fromAddress") String fromAddress,
                               @RequestParam(value = "fromCellphoneNumber") String fromCellphoneNumber,
                              @RequestParam(value = "toAddress") String toAddress,
                              @RequestParam(value = "toCellphoneNumber") String toCellphoneNumber) {


        //functionality to follow
        return  "";
    }

    @GetMapping("/encryptMessage")
    public String loadVoucher(@RequestParam(value = "messageDetails") String messageDetails) {

        //functionality to follow

        return  Logic.encryptMessage(messageDetails);
    }


}
