package com.functions;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("app")
public class AppProperties {

    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public String getLoadVoucherUrl() {
        return loadVoucherUrl;
    }

    public void setLoadVoucherUrl(String loadVoucherUrl) {
        this.loadVoucherUrl = loadVoucherUrl;
    }

    private String registerUrl;
    private String loadVoucherUrl;



}
