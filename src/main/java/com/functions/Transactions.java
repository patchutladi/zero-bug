package com.functions;

public interface Transactions {

    void loadVoucher(String cellPhoneNumber, String voucherNumber);
    void registerUser(String name, String address,String cellPhoneNumber);
    void requestWifi(String fromAddress, String fromCellphoneNumber, String toAddress, String toCellphoneNumber);
    void deductVoucherAmount(String cellPhoneNumber, String voucherNumber);
    void sendConfirmation(String number, String messageToSend);
}
