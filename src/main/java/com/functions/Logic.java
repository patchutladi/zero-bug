package com.functions;

import com.restservice.RegisterUser;
import com.zang.api.configuration.BasicZangConfiguration;
import com.zang.api.connectors.SmsConnector;
import com.zang.api.connectors.ZangConnectorFactory;
import com.zang.api.domain.SmsMessage;
import com.zang.api.exceptions.ZangException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service

public class Logic {


    public static String encryptMessage(String message)
    {
        //functionality to follow
        return "";
    }



    public static String registerUserFunction(RegisterUser registerUser, String url)
    {
        String successfulRegister = "Welcome to WiFiWiNot. Please dial *150# to request a wifi Hotspot";
        String unsuccessfulRegister = "Welcome to WiFiWiNot. Please dial again.";

        String responseMessage =   null;


        RestTemplate restTemplate =new RestTemplate();

        HttpEntity<RegisterUser> request = new HttpEntity(registerUser);

        System.out.println(request.getBody());
        ResponseEntity<RegisterUser> response = restTemplate
                .exchange(url, HttpMethod.POST, request, RegisterUser.class);

        if(response.hasBody())
        {
             //send sms of confirmed registration
            //sendSMS(registerUser.getCellPhoneNumber(),successfulRegister);
            responseMessage = successfulRegister;
        }
        else
        {
            //send sms of unsuccessful registration
            //sendSMS(registerUser.getCellPhoneNumber(),unsuccessfulRegister);
            responseMessage = unsuccessfulRegister;
        }

        return responseMessage;

    }

    public static  String sendSMS(String toNumber, String messageToSend )
    {

        BasicZangConfiguration config = new BasicZangConfiguration();

        String response = null;
        String token = "b59f8980b0de4ccf91116237e1aa87af";
        config.setSid("AC777c3e3207b713dec5b44337b83aeb24");
        config.setAuthToken(token);

        SmsConnector connector = ZangConnectorFactory.getSmsConnector(config);

        //send sms message
        try {
            SmsMessage sentSmsMessage = connector
                    .sendSmsMessage(
                            toNumber,
                            "+15203344762", //mobile provided by Avaya
                            messageToSend,
                            null, null, null);
            System.out.println(sentSmsMessage.getStatus());
            response = sentSmsMessage.getStatus();
        } catch (ZangException e) {
            e.printStackTrace();
            response = e.getMessage();
        }
        return response;
    }
}
